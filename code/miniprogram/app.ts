//app.ts
// import {navigationBarLeftSpace as capsuleSpace} from './config/systemInfo';
import { mixinComponent } from './utils/mixinComponent'
const computedBehavior = require('miniprogram-computed')
App({
  globalData: {
  }
})
// 因为我这里要加入全局globalData，所以在App之后调用了
mixinComponent({
  data: {
    globalData: getApp().globalData,
  },
  behaviors: [computedBehavior.behavior],
  options: {
    multipleSlots: true,
    addGlobalClass: true,
    pureDataPattern: /^_/
  },
})

