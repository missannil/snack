// pages/index/tabPage/firstPage/firstPage.js
import {capsuleRight,capsuleHeight,customNavHeight,systemInfo,capsuleTop} from '../../../../config/systemInfo' 
Component({
  data: {
    initalColor:'#18b357',
    initalTop:'250px',
    capsuleRight,
    capsuleHeight,
    customNavHeight,
    systemInfo,
    capsuleTop,
    swiperList: [{
      name: '',
      id: '001',
      src: "/images/1080.png",
      url: "0000001",
      bgColor: 'red'
    },
    {
      name: '',
      id: '002',
      src: "/images/1080copy.png",
      url: "0000001",
      bgColor: 'green'
    }, {
      name: '',
      id: '003',
      src: "/images/1080copy2.png",
      url: "0000001",
      bgColor: 'yellow'
    }
    ],
    btnLeft: 10,
    btnWidth: 355,
    btnTop: 60,
    initTop: 70,
    iconList: [
      {
        name: '经典小吃',
        id: '0002',
        url: "/images/001.png"
      }, {
        name: '家庭火锅',
        id: '0002',
        url: "/images/002.png"
      }, {
        name: '水果净菜',
        id: '0002',
        url: "/images/003.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/004.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/004.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/004.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/004.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      },
      {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      },
      {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      },
      {
        name: '酒水饮料',
        id: '0002',
        url: "/images/010.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/005.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/006.png"
      }, {
        name: '酒水饮料',
        id: '0002',
        url: "/images/007.png"
      },
    ],
    bannerList: [
      {
        id: '001',
        name: '麻辣烫',
        url: "/images/1080.png"
      },
      {
        id: '002',
        name: '酸辣粉',
        url: "/images/1080.png"
      }, {
        id: '001',
        name: '卤肉饭',
        url: "/images/1080.png"
      },
    ],
    categoryList: [
      {
        categoryId: '00001',
        name: '猜你喜欢',
      },
      {
        categoryId: '00002',
        name: '疯狂折扣',
      },
      {
        categoryId: '00003',
        name: '网红推荐',
      },
      {
        categoryId: '00004',
        name: '盘锦特产',
      }
    ],
    activeTabIndex: 0,
    capsuleSpace: getApp().globalData.capsuleSpace
  },
  properties: {

  },
  computed: {
    top(data: any) {
      return data.initTop
    },
    sum(data: any) {
      return data.a + data.b
    }
  },
  methods: {
    ddd() {
      console.log('move')
    },
    ccc() {
      console.log('end')
    },
    aaa(e) {
      console.log('swiperchange',e)
    },
    bbb(e) {
      console.log(50 - e.detail.scrollTop > 0 ? 50 - e.detail.scrollTop : 0)
      this.setData({
        initTop: 50 - e.detail.scrollTop > 0 ? 50 - e.detail.scrollTop : 0
      })
    }
  },
  lifetimes: {
    attached()  {

    }
  }
})

