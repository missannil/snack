import { capsuleRight, capsuleHeight, customNavHeight, systemInfo, capsuleTop } from '../../../../config/systemInfo';
Component({
    data: {
        initalColor: '#18b357',
        initalTop: '250px',
        capsuleRight,
        capsuleHeight,
        customNavHeight,
        systemInfo,
        capsuleTop,
        swiperList: [{
                name: '',
                id: '001',
                src: "/images/1080.png",
                url: "0000001",
                bgColor: 'red'
            },
            {
                name: '',
                id: '002',
                src: "/images/1080copy.png",
                url: "0000001",
                bgColor: 'green'
            }, {
                name: '',
                id: '003',
                src: "/images/1080copy2.png",
                url: "0000001",
                bgColor: 'yellow'
            }
        ],
        btnLeft: 10,
        btnWidth: 355,
        btnTop: 60,
        initTop: 70,
        iconList: [
            {
                name: '经典小吃',
                id: '0002',
                url: "/images/001.png"
            }, {
                name: '家庭火锅',
                id: '0002',
                url: "/images/002.png"
            }, {
                name: '水果净菜',
                id: '0002',
                url: "/images/003.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/004.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/004.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/004.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/004.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            },
            {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            },
            {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            },
            {
                name: '酒水饮料',
                id: '0002',
                url: "/images/010.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/005.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/006.png"
            }, {
                name: '酒水饮料',
                id: '0002',
                url: "/images/007.png"
            },
        ],
        bannerList: [
            {
                id: '001',
                name: '麻辣烫',
                url: "/images/1080.png"
            },
            {
                id: '002',
                name: '酸辣粉',
                url: "/images/1080.png"
            }, {
                id: '001',
                name: '卤肉饭',
                url: "/images/1080.png"
            },
        ],
        categoryList: [
            {
                categoryId: '00001',
                name: '猜你喜欢',
            },
            {
                categoryId: '00002',
                name: '疯狂折扣',
            },
            {
                categoryId: '00003',
                name: '网红推荐',
            },
            {
                categoryId: '00004',
                name: '盘锦特产',
            }
        ],
        activeTabIndex: 0,
        capsuleSpace: getApp().globalData.capsuleSpace
    },
    properties: {},
    computed: {
        top(data) {
            return data.initTop;
        },
        sum(data) {
            return data.a + data.b;
        }
    },
    methods: {
        ddd() {
            console.log('move');
        },
        ccc() {
            console.log('end');
        },
        aaa(e) {
            console.log('swiperchange', e);
        },
        bbb(e) {
            console.log(50 - e.detail.scrollTop > 0 ? 50 - e.detail.scrollTop : 0);
            this.setData({
                initTop: 50 - e.detail.scrollTop > 0 ? 50 - e.detail.scrollTop : 0
            });
        }
    },
    lifetimes: {
        attached() {
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlyc3RQYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZmlyc3RQYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBQyxZQUFZLEVBQUMsYUFBYSxFQUFDLGVBQWUsRUFBQyxVQUFVLEVBQUMsVUFBVSxFQUFDLE1BQU0sK0JBQStCLENBQUE7QUFDOUcsU0FBUyxDQUFDO0lBQ1IsSUFBSSxFQUFFO1FBQ0osV0FBVyxFQUFDLFNBQVM7UUFDckIsU0FBUyxFQUFDLE9BQU87UUFDakIsWUFBWTtRQUNaLGFBQWE7UUFDYixlQUFlO1FBQ2YsVUFBVTtRQUNWLFVBQVU7UUFDVixVQUFVLEVBQUUsQ0FBQztnQkFDWCxJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsa0JBQWtCO2dCQUN2QixHQUFHLEVBQUUsU0FBUztnQkFDZCxPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLHNCQUFzQjtnQkFDM0IsR0FBRyxFQUFFLFNBQVM7Z0JBQ2QsT0FBTyxFQUFFLE9BQU87YUFDakIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsdUJBQXVCO2dCQUM1QixHQUFHLEVBQUUsU0FBUztnQkFDZCxPQUFPLEVBQUUsUUFBUTthQUNsQjtTQUNBO1FBQ0QsT0FBTyxFQUFFLEVBQUU7UUFDWCxRQUFRLEVBQUUsR0FBRztRQUNiLE1BQU0sRUFBRSxFQUFFO1FBQ1YsT0FBTyxFQUFFLEVBQUU7UUFDWCxRQUFRLEVBQUU7WUFDUjtnQkFDRSxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QjtZQUNEO2dCQUNFLElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkI7WUFDRDtnQkFDRSxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QixFQUFFO2dCQUNELElBQUksRUFBRSxNQUFNO2dCQUNaLEVBQUUsRUFBRSxNQUFNO2dCQUNWLEdBQUcsRUFBRSxpQkFBaUI7YUFDdkIsRUFBRTtnQkFDRCxJQUFJLEVBQUUsTUFBTTtnQkFDWixFQUFFLEVBQUUsTUFBTTtnQkFDVixHQUFHLEVBQUUsaUJBQWlCO2FBQ3ZCLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE1BQU07Z0JBQ1osRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLGlCQUFpQjthQUN2QjtTQUNGO1FBQ0QsVUFBVSxFQUFFO1lBQ1Y7Z0JBQ0UsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsR0FBRyxFQUFFLGtCQUFrQjthQUN4QjtZQUNEO2dCQUNFLEVBQUUsRUFBRSxLQUFLO2dCQUNULElBQUksRUFBRSxLQUFLO2dCQUNYLEdBQUcsRUFBRSxrQkFBa0I7YUFDeEIsRUFBRTtnQkFDRCxFQUFFLEVBQUUsS0FBSztnQkFDVCxJQUFJLEVBQUUsS0FBSztnQkFDWCxHQUFHLEVBQUUsa0JBQWtCO2FBQ3hCO1NBQ0Y7UUFDRCxZQUFZLEVBQUU7WUFDWjtnQkFDRSxVQUFVLEVBQUUsT0FBTztnQkFDbkIsSUFBSSxFQUFFLE1BQU07YUFDYjtZQUNEO2dCQUNFLFVBQVUsRUFBRSxPQUFPO2dCQUNuQixJQUFJLEVBQUUsTUFBTTthQUNiO1lBQ0Q7Z0JBQ0UsVUFBVSxFQUFFLE9BQU87Z0JBQ25CLElBQUksRUFBRSxNQUFNO2FBQ2I7WUFDRDtnQkFDRSxVQUFVLEVBQUUsT0FBTztnQkFDbkIsSUFBSSxFQUFFLE1BQU07YUFDYjtTQUNGO1FBQ0QsY0FBYyxFQUFFLENBQUM7UUFDakIsWUFBWSxFQUFFLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZO0tBQy9DO0lBQ0QsVUFBVSxFQUFFLEVBRVg7SUFDRCxRQUFRLEVBQUU7UUFDUixHQUFHLENBQUMsSUFBUztZQUNYLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQTtRQUNyQixDQUFDO1FBQ0QsR0FBRyxDQUFDLElBQVM7WUFDWCxPQUFPLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQTtRQUN4QixDQUFDO0tBQ0Y7SUFDRCxPQUFPLEVBQUU7UUFDUCxHQUFHO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUNyQixDQUFDO1FBQ0QsR0FBRztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDcEIsQ0FBQztRQUNELEdBQUcsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFDLENBQUE7UUFDL0IsQ0FBQztRQUNELEdBQUcsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ3RFLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ1gsT0FBTyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuRSxDQUFDLENBQUE7UUFDSixDQUFDO0tBQ0Y7SUFDRCxTQUFTLEVBQUU7UUFDVCxRQUFRO1FBRVIsQ0FBQztLQUNGO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gcGFnZXMvaW5kZXgvdGFiUGFnZS9maXJzdFBhZ2UvZmlyc3RQYWdlLmpzXHJcbmltcG9ydCB7Y2Fwc3VsZVJpZ2h0LGNhcHN1bGVIZWlnaHQsY3VzdG9tTmF2SGVpZ2h0LHN5c3RlbUluZm8sY2Fwc3VsZVRvcH0gZnJvbSAnLi4vLi4vLi4vLi4vY29uZmlnL3N5c3RlbUluZm8nIFxyXG5Db21wb25lbnQoe1xyXG4gIGRhdGE6IHtcclxuICAgIGluaXRhbENvbG9yOicjMThiMzU3JyxcclxuICAgIGluaXRhbFRvcDonMjUwcHgnLFxyXG4gICAgY2Fwc3VsZVJpZ2h0LFxyXG4gICAgY2Fwc3VsZUhlaWdodCxcclxuICAgIGN1c3RvbU5hdkhlaWdodCxcclxuICAgIHN5c3RlbUluZm8sXHJcbiAgICBjYXBzdWxlVG9wLFxyXG4gICAgc3dpcGVyTGlzdDogW3tcclxuICAgICAgbmFtZTogJycsXHJcbiAgICAgIGlkOiAnMDAxJyxcclxuICAgICAgc3JjOiBcIi9pbWFnZXMvMTA4MC5wbmdcIixcclxuICAgICAgdXJsOiBcIjAwMDAwMDFcIixcclxuICAgICAgYmdDb2xvcjogJ3JlZCdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6ICcnLFxyXG4gICAgICBpZDogJzAwMicsXHJcbiAgICAgIHNyYzogXCIvaW1hZ2VzLzEwODBjb3B5LnBuZ1wiLFxyXG4gICAgICB1cmw6IFwiMDAwMDAwMVwiLFxyXG4gICAgICBiZ0NvbG9yOiAnZ3JlZW4nXHJcbiAgICB9LCB7XHJcbiAgICAgIG5hbWU6ICcnLFxyXG4gICAgICBpZDogJzAwMycsXHJcbiAgICAgIHNyYzogXCIvaW1hZ2VzLzEwODBjb3B5Mi5wbmdcIixcclxuICAgICAgdXJsOiBcIjAwMDAwMDFcIixcclxuICAgICAgYmdDb2xvcjogJ3llbGxvdydcclxuICAgIH1cclxuICAgIF0sXHJcbiAgICBidG5MZWZ0OiAxMCxcclxuICAgIGJ0bldpZHRoOiAzNTUsXHJcbiAgICBidG5Ub3A6IDYwLFxyXG4gICAgaW5pdFRvcDogNzAsXHJcbiAgICBpY29uTGlzdDogW1xyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ+e7j+WFuOWwj+WQgycsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDEucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICflrrbluq3ngavplIUnLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDAyLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn5rC05p6c5YeA6I+cJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwMy5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDQucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDEwLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNS5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDYucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA3LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNC5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMTAucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA1LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNi5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDcucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA0LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAxMC5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDUucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA2LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNy5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDQucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDEwLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNS5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDYucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA3LnBuZ1wiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAxMC5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDUucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA2LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNy5wbmdcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMTAucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA1LnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNi5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDcucG5nXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDEwLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAn6YWS5rC06aWu5paZJyxcclxuICAgICAgICBpZDogJzAwMDInLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzAwNS5wbmdcIlxyXG4gICAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ+mFkuawtOmlruaWmScsXHJcbiAgICAgICAgaWQ6ICcwMDAyJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8wMDYucG5nXCJcclxuICAgICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICfphZLmsLTppa7mlpknLFxyXG4gICAgICAgIGlkOiAnMDAwMicsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMDA3LnBuZ1wiXHJcbiAgICAgIH0sXHJcbiAgICBdLFxyXG4gICAgYmFubmVyTGlzdDogW1xyXG4gICAgICB7XHJcbiAgICAgICAgaWQ6ICcwMDEnLFxyXG4gICAgICAgIG5hbWU6ICfpurvovqPng6snLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzEwODAucG5nXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGlkOiAnMDAyJyxcclxuICAgICAgICBuYW1lOiAn6YW46L6j57KJJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8xMDgwLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBpZDogJzAwMScsXHJcbiAgICAgICAgbmFtZTogJ+WNpOiCiemlrScsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMTA4MC5wbmdcIlxyXG4gICAgICB9LFxyXG4gICAgXSxcclxuICAgIGNhdGVnb3J5TGlzdDogW1xyXG4gICAgICB7XHJcbiAgICAgICAgY2F0ZWdvcnlJZDogJzAwMDAxJyxcclxuICAgICAgICBuYW1lOiAn54yc5L2g5Zac5qyiJyxcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGNhdGVnb3J5SWQ6ICcwMDAwMicsXHJcbiAgICAgICAgbmFtZTogJ+eWr+eLguaKmOaJoycsXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBjYXRlZ29yeUlkOiAnMDAwMDMnLFxyXG4gICAgICAgIG5hbWU6ICfnvZHnuqLmjqjojZAnLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgY2F0ZWdvcnlJZDogJzAwMDA0JyxcclxuICAgICAgICBuYW1lOiAn55uY6ZSm54m55LqnJyxcclxuICAgICAgfVxyXG4gICAgXSxcclxuICAgIGFjdGl2ZVRhYkluZGV4OiAwLFxyXG4gICAgY2Fwc3VsZVNwYWNlOiBnZXRBcHAoKS5nbG9iYWxEYXRhLmNhcHN1bGVTcGFjZVxyXG4gIH0sXHJcbiAgcHJvcGVydGllczoge1xyXG5cclxuICB9LFxyXG4gIGNvbXB1dGVkOiB7XHJcbiAgICB0b3AoZGF0YTogYW55KSB7XHJcbiAgICAgIHJldHVybiBkYXRhLmluaXRUb3BcclxuICAgIH0sXHJcbiAgICBzdW0oZGF0YTogYW55KSB7XHJcbiAgICAgIHJldHVybiBkYXRhLmEgKyBkYXRhLmJcclxuICAgIH1cclxuICB9LFxyXG4gIG1ldGhvZHM6IHtcclxuICAgIGRkZCgpIHtcclxuICAgICAgY29uc29sZS5sb2coJ21vdmUnKVxyXG4gICAgfSxcclxuICAgIGNjYygpIHtcclxuICAgICAgY29uc29sZS5sb2coJ2VuZCcpXHJcbiAgICB9LFxyXG4gICAgYWFhKGUpIHtcclxuICAgICAgY29uc29sZS5sb2coJ3N3aXBlcmNoYW5nZScsZSlcclxuICAgIH0sXHJcbiAgICBiYmIoZSkge1xyXG4gICAgICBjb25zb2xlLmxvZyg1MCAtIGUuZGV0YWlsLnNjcm9sbFRvcCA+IDAgPyA1MCAtIGUuZGV0YWlsLnNjcm9sbFRvcCA6IDApXHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgaW5pdFRvcDogNTAgLSBlLmRldGFpbC5zY3JvbGxUb3AgPiAwID8gNTAgLSBlLmRldGFpbC5zY3JvbGxUb3AgOiAwXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfSxcclxuICBsaWZldGltZXM6IHtcclxuICAgIGF0dGFjaGVkKCkgIHtcclxuXHJcbiAgICB9XHJcbiAgfVxyXG59KVxyXG5cclxuIl19