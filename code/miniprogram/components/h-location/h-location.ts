// components/h-location/h-location.js
import {shopInfo} from '../../config/shopInfo'
Component({
  properties:{
    url:{
      type:String,
      value:'/pages/shopLocation/shopLocation'
    },
    iconClass:{
      type:String,
      value:'i-weizhi'
    },
    iconSize:{
      type:String,
      value:'36'
    }
  },
  data: {
    community:shopInfo.community,
  }
})
