import { shopInfo } from '../../config/shopInfo';
Component({
    properties: {
        url: {
            type: String,
            value: '/pages/shopLocation/shopLocation'
        },
        iconClass: {
            type: String,
            value: 'i-weizhi'
        },
        iconSize: {
            type: String,
            value: '36'
        }
    },
    data: {
        community: shopInfo.community,
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaC1sb2NhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImgtbG9jYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLHVCQUF1QixDQUFBO0FBQzlDLFNBQVMsQ0FBQztJQUNSLFVBQVUsRUFBQztRQUNULEdBQUcsRUFBQztZQUNGLElBQUksRUFBQyxNQUFNO1lBQ1gsS0FBSyxFQUFDLGtDQUFrQztTQUN6QztRQUNELFNBQVMsRUFBQztZQUNSLElBQUksRUFBQyxNQUFNO1lBQ1gsS0FBSyxFQUFDLFVBQVU7U0FDakI7UUFDRCxRQUFRLEVBQUM7WUFDUCxJQUFJLEVBQUMsTUFBTTtZQUNYLEtBQUssRUFBQyxJQUFJO1NBQ1g7S0FDRjtJQUNELElBQUksRUFBRTtRQUNKLFNBQVMsRUFBQyxRQUFRLENBQUMsU0FBUztLQUM3QjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8vIGNvbXBvbmVudHMvaC1sb2NhdGlvbi9oLWxvY2F0aW9uLmpzXHJcbmltcG9ydCB7c2hvcEluZm99IGZyb20gJy4uLy4uL2NvbmZpZy9zaG9wSW5mbydcclxuQ29tcG9uZW50KHtcclxuICBwcm9wZXJ0aWVzOntcclxuICAgIHVybDp7XHJcbiAgICAgIHR5cGU6U3RyaW5nLFxyXG4gICAgICB2YWx1ZTonL3BhZ2VzL3Nob3BMb2NhdGlvbi9zaG9wTG9jYXRpb24nXHJcbiAgICB9LFxyXG4gICAgaWNvbkNsYXNzOntcclxuICAgICAgdHlwZTpTdHJpbmcsXHJcbiAgICAgIHZhbHVlOidpLXdlaXpoaSdcclxuICAgIH0sXHJcbiAgICBpY29uU2l6ZTp7XHJcbiAgICAgIHR5cGU6U3RyaW5nLFxyXG4gICAgICB2YWx1ZTonMzYnXHJcbiAgICB9XHJcbiAgfSxcclxuICBkYXRhOiB7XHJcbiAgICBjb21tdW5pdHk6c2hvcEluZm8uY29tbXVuaXR5LFxyXG4gIH1cclxufSlcclxuIl19