// components/h-scaleBox/h-scaleBox.ts
Component({
  properties: {
    scale: {
      type: String,
      value: ''
    }
  },
  data: {
    paddingTop: '56.25%', //16:9
  },
  observers: {
    scale(scale) {
      if (scale) {
        const tempArr = scale.split(':')
        this.setData({
          paddingTop: (tempArr[1] / tempArr[0] * 100).toFixed(8) + '%'
        })
      }
    }
  }
})