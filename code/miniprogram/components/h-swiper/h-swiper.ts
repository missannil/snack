// import { getGlobalData }from "../../utils/getGlobalData"
Component({
  properties: {
    height: {
      type: String,
      value: '400'
    },
    indicatorColor: {
      type: String,
      value: '#aaa'
    },
    activeColor: {
      type: String,
      value: '#18b357'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    bannerList: [
      {
        id: '001',
        name: '麻辣烫',
        url: "/images/1080.png"
      },
      {
        id: '002',
        name: '酸辣粉',
        url: "/images/1080.png"
      }, {
        id: '001',
        name: '卤肉饭',
        url: "/images/1080.png"
      },
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  lifetimes: {
    attached() {
     
    }
  }
})
