Component({
    properties: {
        height: {
            type: String,
            value: '400'
        },
        indicatorColor: {
            type: String,
            value: '#aaa'
        },
        activeColor: {
            type: String,
            value: '#18b357'
        }
    },
    data: {
        bannerList: [
            {
                id: '001',
                name: '麻辣烫',
                url: "/images/1080.png"
            },
            {
                id: '002',
                name: '酸辣粉',
                url: "/images/1080.png"
            }, {
                id: '001',
                name: '卤肉饭',
                url: "/images/1080.png"
            },
        ]
    },
    methods: {},
    lifetimes: {
        attached() {
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaC1zd2lwZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoLXN3aXBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxTQUFTLENBQUM7SUFDUixVQUFVLEVBQUU7UUFDVixNQUFNLEVBQUU7WUFDTixJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxLQUFLO1NBQ2I7UUFDRCxjQUFjLEVBQUU7WUFDZCxJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxNQUFNO1NBQ2Q7UUFDRCxXQUFXLEVBQUU7WUFDWCxJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxTQUFTO1NBQ2pCO0tBQ0Y7SUFJRCxJQUFJLEVBQUU7UUFDSixVQUFVLEVBQUU7WUFDVjtnQkFDRSxFQUFFLEVBQUUsS0FBSztnQkFDVCxJQUFJLEVBQUUsS0FBSztnQkFDWCxHQUFHLEVBQUUsa0JBQWtCO2FBQ3hCO1lBQ0Q7Z0JBQ0UsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsR0FBRyxFQUFFLGtCQUFrQjthQUN4QixFQUFFO2dCQUNELEVBQUUsRUFBRSxLQUFLO2dCQUNULElBQUksRUFBRSxLQUFLO2dCQUNYLEdBQUcsRUFBRSxrQkFBa0I7YUFDeEI7U0FDRjtLQUNGO0lBS0QsT0FBTyxFQUFFLEVBRVI7SUFDRCxTQUFTLEVBQUU7UUFDVCxRQUFRO1FBRVIsQ0FBQztLQUNGO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW1wb3J0IHsgZ2V0R2xvYmFsRGF0YSB9ZnJvbSBcIi4uLy4uL3V0aWxzL2dldEdsb2JhbERhdGFcIlxyXG5Db21wb25lbnQoe1xyXG4gIHByb3BlcnRpZXM6IHtcclxuICAgIGhlaWdodDoge1xyXG4gICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgIHZhbHVlOiAnNDAwJ1xyXG4gICAgfSxcclxuICAgIGluZGljYXRvckNvbG9yOiB7XHJcbiAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgdmFsdWU6ICcjYWFhJ1xyXG4gICAgfSxcclxuICAgIGFjdGl2ZUNvbG9yOiB7XHJcbiAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgdmFsdWU6ICcjMThiMzU3J1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5Yid5aeL5pWw5o2uXHJcbiAgICovXHJcbiAgZGF0YToge1xyXG4gICAgYmFubmVyTGlzdDogW1xyXG4gICAgICB7XHJcbiAgICAgICAgaWQ6ICcwMDEnLFxyXG4gICAgICAgIG5hbWU6ICfpurvovqPng6snLFxyXG4gICAgICAgIHVybDogXCIvaW1hZ2VzLzEwODAucG5nXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGlkOiAnMDAyJyxcclxuICAgICAgICBuYW1lOiAn6YW46L6j57KJJyxcclxuICAgICAgICB1cmw6IFwiL2ltYWdlcy8xMDgwLnBuZ1wiXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBpZDogJzAwMScsXHJcbiAgICAgICAgbmFtZTogJ+WNpOiCiemlrScsXHJcbiAgICAgICAgdXJsOiBcIi9pbWFnZXMvMTA4MC5wbmdcIlxyXG4gICAgICB9LFxyXG4gICAgXVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOaWueazleWIl+ihqFxyXG4gICAqL1xyXG4gIG1ldGhvZHM6IHtcclxuXHJcbiAgfSxcclxuICBsaWZldGltZXM6IHtcclxuICAgIGF0dGFjaGVkKCkge1xyXG4gICAgIFxyXG4gICAgfVxyXG4gIH1cclxufSlcclxuIl19