// components/h-categoryIcon/h-categoryIcon.js


Component({
  /**
   * 组件的属性列表
   */
  properties: {
    iconList:{
      type:Array,
      value:[]
    },
    count:{
      type:Number,
      value:5
    },
    space:{
      type:String,
      value:'20'
    },
    size:{
      type:String,
      value:'100'
    },
    font:{
      type:String,
      value:'24'
    }
  },
  observers:{
    "count":function(num){
      this.setData({
        percent:100/num 
      })
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    percent:'20'
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
