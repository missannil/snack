Component({
    properties: {
        tabbarList: {
            type: Array,
            value: []
        },
        safe: {
            type: Boolean,
            value: false
        },
        space: {
            type: Number,
            value: 12
        },
        activeTabIndex: {
            type: Number
        },
        iconSize: {
            type: Number,
            value: 36
        },
        textSize: {
            type: Number,
            value: 24
        }
    },
    methods: {
        changeIndex(e) {
            const activeTabIndex = e.currentTarget.dataset.index;
            if (activeTabIndex !== this.data.activeTabIndex) {
                this.setData({
                    activeTabIndex
                });
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaC10YWJiYXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoLXRhYmJhci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxTQUFTLENBQUM7SUFDUixVQUFVLEVBQUU7UUFDVixVQUFVLEVBQUU7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLEtBQUssRUFBRSxFQUFFO1NBQ1Y7UUFDRCxJQUFJLEVBQUM7WUFDSCxJQUFJLEVBQUMsT0FBTztZQUNaLEtBQUssRUFBQyxLQUFLO1NBQ1o7UUFDRCxLQUFLLEVBQUU7WUFDTCxJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxFQUFFO1NBQ1Y7UUFDRCxjQUFjLEVBQUU7WUFDZCxJQUFJLEVBQUUsTUFBTTtTQUNiO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsSUFBSSxFQUFFLE1BQU07WUFDWixLQUFLLEVBQUUsRUFBRTtTQUNWO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsSUFBSSxFQUFFLE1BQU07WUFDWixLQUFLLEVBQUUsRUFBRTtTQUNWO0tBQ0Y7SUFDRCxPQUFPLEVBQUU7UUFDUCxXQUFXLENBQUMsQ0FBQztZQUNYLE1BQU0sY0FBYyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQTtZQUNwRCxJQUFJLGNBQWMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLE9BQU8sQ0FBQztvQkFDWCxjQUFjO2lCQUNmLENBQUMsQ0FBQTthQUNIO1FBQ0gsQ0FBQztLQUNGO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLy8gY29tcG9uZW50cy9oLXRhYmJhci9pbmRleC50c1xyXG5Db21wb25lbnQoe1xyXG4gIHByb3BlcnRpZXM6IHtcclxuICAgIHRhYmJhckxpc3Q6IHtcclxuICAgICAgdHlwZTogQXJyYXksXHJcbiAgICAgIHZhbHVlOiBbXVxyXG4gICAgfSxcclxuICAgIHNhZmU6e1xyXG4gICAgICB0eXBlOkJvb2xlYW4sXHJcbiAgICAgIHZhbHVlOmZhbHNlXHJcbiAgICB9LFxyXG4gICAgc3BhY2U6IHsgLy/kuIrkuIvpl7Tot51cclxuICAgICAgdHlwZTogTnVtYmVyLFxyXG4gICAgICB2YWx1ZTogMTIgICAgIFxyXG4gICAgfSxcclxuICAgIGFjdGl2ZVRhYkluZGV4OiB7IC8v5rS75Yqo57Si5byVXHJcbiAgICAgIHR5cGU6IE51bWJlclxyXG4gICAgfSxcclxuICAgIGljb25TaXplOiB7IC8v5Zu+54mH5a2X5L2T5aSn5bCPXHJcbiAgICAgIHR5cGU6IE51bWJlcixcclxuICAgICAgdmFsdWU6IDM2XHJcbiAgICB9LFxyXG4gICAgdGV4dFNpemU6IHsgLy/mloflrZflrZfkvZPlpKflsI9cclxuICAgICAgdHlwZTogTnVtYmVyLFxyXG4gICAgICB2YWx1ZTogMjRcclxuICAgIH1cclxuICB9LFxyXG4gIG1ldGhvZHM6IHtcclxuICAgIGNoYW5nZUluZGV4KGUpIHsgLy/mlLnlj5jntKLlvJVcclxuICAgICAgY29uc3QgYWN0aXZlVGFiSW5kZXggPSBlLmN1cnJlbnRUYXJnZXQuZGF0YXNldC5pbmRleCBcclxuICAgICAgaWYgKGFjdGl2ZVRhYkluZGV4ICE9PSB0aGlzLmRhdGEuYWN0aXZlVGFiSW5kZXgpIHtcclxuICAgICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgICAgYWN0aXZlVGFiSW5kZXhcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59KVxyXG4iXX0=