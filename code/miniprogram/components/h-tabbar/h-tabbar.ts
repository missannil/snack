// components/h-tabbar/index.ts
Component({
  properties: {
    tabbarList: {
      type: Array,
      value: []
    },
    safe:{
      type:Boolean,
      value:false
    },
    space: { //上下间距
      type: Number,
      value: 12     
    },
    activeTabIndex: { //活动索引
      type: Number
    },
    iconSize: { //图片字体大小
      type: Number,
      value: 36
    },
    textSize: { //文字字体大小
      type: Number,
      value: 24
    }
  },
  methods: {
    changeIndex(e) { //改变索引
      const activeTabIndex = e.currentTarget.dataset.index 
      if (activeTabIndex !== this.data.activeTabIndex) {
        this.setData({
          activeTabIndex
        })
      }
    }
  }
})
