## 自定义组件 h-tabbar 描述文档
1. 用途: 底部自定义的tabbar
2. 功能描述: 点击不同tab图标,改变activeTabIndex,兼容苹果safeArea。
   外部通过model:active-tab-index="{{activeTabIndex}}}"与内部activeTabIndex双向绑定
3. 内部组件: 无
4. 外部工具/插件: 无
5. 属性传值

| properties属性 |描述| 默认值  |  类型 | 是否必传 |
|  :----------: | :----: | :---: | :------:|:------:|
|tabbarList    | 引入的tabbar配置 | undefined | 对象   |  必须 |
|activeTabIndex| 当前活动的tab索引 | undefined | Number |  必须  |
|space| 字体 icon 和上下边的距离 | 12 | Number |  非  |
|textSize| 文字大小| 24 | Number |  非  |
|iconSize| icon大小| 36 | Number |  非  |
|safe| 让出iphone的home条| false | Boolean |  非  |


6. 使用示例

```html 
<!-- wxml -->
  <h-tabbar safe tabbarList="{{tabbarList}}" model:active-tab-index="{{activeTabIndex}}"/>

```
```js
    // js
    import { tabbarList } from "../../config/tabbarList"
    Page({
        data: {
            tabbarList,
            activeTabIndex: 0
        },
    }) 
    
```

                  