## 自定义组件 h-topNav 描述
1. 用途: 放置在页面顶部自定义导航栏
2. 功能描述: 自适应顶部，通过padding空出状态栏,长度为66%让出右边capsule 胶囊
3. 内部组件: 无
4. 外部工具/插件: 需要引入系统信息```const systemInfo = wx.getSystemInfoSync()```
5.  属性传值

| properties属性 |描述| 默认值  |  类型 | 是否必传 |
|  :----------: | :----: | :---: | :------:|:------:|
|showBack    | 是否显示返回(自动屏蔽slot) | false | 布尔   |  非 |
|title    | 中间文字| 空 | string   |  非 |
|titleSize| 中间文字大小 | 36 | String |  非  |
|titleColor| 中间文字颜色| 空 | #18b357 |  非  |
|backSize| 左面返回的字体大小 | 空 | string |  非  |
|backColor| 左面返回的字体颜色| 空 | #333 |  非  |
|color    | 外层颜色 | 空 | string  |  非 |
|bgColor    | 外层背景色 | 空 | string  |  非 |
|fontSize    | 外层字体大小 | 空 | string  |  非 |
|slot    | showBack为false时可显示slot内容 | 空 | slot  |  非 |

6. 使用示例

```html 
<!-- 左部分用slot -->
    <h-topNav  >  
        <h-location />
    </h-topNav> 
    <!-- 左部分用默认放回按钮  -->
    <h-topNav showBack title="首页" />  
```
<!-- 顶部自定义的导航栏 
1. 功能描述: 自适应不同型号手机高度的上方导航栏。
2. 内部组件: 无
3. 外部工具/插件: config 里面的 /systemInfo.js (statusBarHeight, navigationBarHeight)
4. 可传入属性(properties):{
    1. 默认宽高 width:100% height:auto (不同型号手机不同)
    1. showBack            (是否出现返回按钮) 默认不显示    不是必须
    2. backSize titleSize  (返回和title 文字大小)，默认 32 36 number 单位rpx 不是必须
    3. title (中间内容文字)  默认 '' string  必须
    4. titleColor(中间title文字颜色) 默认'var(--theme-text-color)'string 不是必须
    5. bgColor(导航栏背景颜色) 默认'var(--theme-text-color)'string 不是必须
    6. backColor(返回文字颜色)默认'var(--theme-text-color)'string 不是必须
5. 示例 <h-topNav showBack  title="{{tabbarList[active].title}}" /> -->
                  