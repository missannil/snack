// components/cu-navigation/cu-navigation.js
import {statusBarHeight,capsuleRight,customNavHeight} from '../../config/systemInfo';
Component({
  data: {
    statusBarHeight,
    customNavHeight,
    capsuleRight,
  },
  properties: {
    fontSize:String,
    showBack:Boolean,
    color:String,
    bgColor:String,
    titleSize: {
      type: String,
      value: '36'
    },
    backSize:String,
    title:String,
    titleColor:String,
    backColor: String
  }
})
