import { statusBarHeight, capsuleRight, customNavHeight } from '../../config/systemInfo';
Component({
    data: {
        statusBarHeight,
        customNavHeight,
        capsuleRight,
    },
    properties: {
        fontSize: String,
        showBack: Boolean,
        color: String,
        bgColor: String,
        titleSize: {
            type: String,
            value: '36'
        },
        backSize: String,
        title: String,
        titleColor: String,
        backColor: String
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaC10b3BOYXYuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJoLXRvcE5hdi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUMsZUFBZSxFQUFDLFlBQVksRUFBQyxlQUFlLEVBQUMsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRixTQUFTLENBQUM7SUFDUixJQUFJLEVBQUU7UUFDSixlQUFlO1FBQ2YsZUFBZTtRQUNmLFlBQVk7S0FDYjtJQUNELFVBQVUsRUFBRTtRQUNWLFFBQVEsRUFBQyxNQUFNO1FBQ2YsUUFBUSxFQUFDLE9BQU87UUFDaEIsS0FBSyxFQUFDLE1BQU07UUFDWixPQUFPLEVBQUMsTUFBTTtRQUNkLFNBQVMsRUFBRTtZQUNULElBQUksRUFBRSxNQUFNO1lBQ1osS0FBSyxFQUFFLElBQUk7U0FDWjtRQUNELFFBQVEsRUFBQyxNQUFNO1FBQ2YsS0FBSyxFQUFDLE1BQU07UUFDWixVQUFVLEVBQUMsTUFBTTtRQUNqQixTQUFTLEVBQUUsTUFBTTtLQUNsQjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8vIGNvbXBvbmVudHMvY3UtbmF2aWdhdGlvbi9jdS1uYXZpZ2F0aW9uLmpzXHJcbmltcG9ydCB7c3RhdHVzQmFySGVpZ2h0LGNhcHN1bGVSaWdodCxjdXN0b21OYXZIZWlnaHR9IGZyb20gJy4uLy4uL2NvbmZpZy9zeXN0ZW1JbmZvJztcclxuQ29tcG9uZW50KHtcclxuICBkYXRhOiB7XHJcbiAgICBzdGF0dXNCYXJIZWlnaHQsXHJcbiAgICBjdXN0b21OYXZIZWlnaHQsXHJcbiAgICBjYXBzdWxlUmlnaHQsXHJcbiAgfSxcclxuICBwcm9wZXJ0aWVzOiB7XHJcbiAgICBmb250U2l6ZTpTdHJpbmcsXHJcbiAgICBzaG93QmFjazpCb29sZWFuLFxyXG4gICAgY29sb3I6U3RyaW5nLFxyXG4gICAgYmdDb2xvcjpTdHJpbmcsXHJcbiAgICB0aXRsZVNpemU6IHtcclxuICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICB2YWx1ZTogJzM2J1xyXG4gICAgfSxcclxuICAgIGJhY2tTaXplOlN0cmluZyxcclxuICAgIHRpdGxlOlN0cmluZyxcclxuICAgIHRpdGxlQ29sb3I6U3RyaW5nLFxyXG4gICAgYmFja0NvbG9yOiBTdHJpbmdcclxuICB9XHJcbn0pXHJcbiJdfQ==