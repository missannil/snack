// components/h-searchBtn/h-searchBtn.js
import {capsuleTop, capsuleRight } from '../../config/systemInfo';
Component({
  properties: {
    isSticky: Boolean,
    zIndex: String,
    left: String,
    top: {
      type:String,
      value: capsuleTop + 'px'
    },
    fontSize: String,
    url: {
      type: String,
      value: '/pages/search/search'
    },
    margin: {
      type: String,
      value: '0 ' + capsuleRight + 'px'
    },
    height: {
      type: String,
      value: "30px"
    },
    width: String,
    text: {
      type: String,
      value: '搜索一下'
    },

  },
  computed: {
    radius(data) {
      return parseInt(data.height) / 2 + (data.height.includes('rpx') ? 'rpx' : 'px')
    },
    lineHeight(data) {
      return parseInt(data.height) + (data.height.includes('rpx') ? 'rpx' : 'px')
    }
  },
  /**
   * 组件的初始数据
   */

  /**
   * 组件的方法列表
   */
  methods: {

  },
  lifetimes:{
    attached(){
      console.log(this.data.radius)
    }
  }
})
