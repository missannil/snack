## 自定义组件 h-topNav 描述
1. 用途: 放置在页面顶部自定义导航栏
2. 功能描述: 自适应顶部，通过padding空出状态栏,长度为66%空出右边capsule 胶囊
3. 内部组件: 无
4. 外部工具/插件: 需要引入系统信息```const systemInfo = wx.getSystemInfoSync()```
5.  属性传值

<!-- | properties属性 |描述| 默认值  |  类型 | 是否必传 |
|  :----------: | :----: | :---: | :------:|:------:|
|zIndex    | 定位层级数  | 空  | number   |  非 |
|width    | 宽度| 100% | string  |  非 |
|isAbsolute | 是否定位| false | boolean  |  非 |
|left| 定位left | 空 | String |  非  |
|top| 定位top | 空 | String |  非  |
|bgColor| 背景色类名| bg-eee | string |  非  |
|url| 跳转链接url| '/pages/search/search' | string |  非  |
|color| 字体颜色类名| color-lightblack | string |  非  |
|text | 搜索hotwords | 搜索一下 | string  |  非 |
|fontSize | 字体大小 | 继承 | string  |  非 |
|iconClass    | 搜索图标class | i-sousuo | string  |  非 | 
|height    | 高度 | 60 | string  |  非 | -->
6. 使用示例

```html 
<!-- wxml -->
   <h-searchBtn iconClass="i-weizhi" text="仆仆以下" textSize="40" iconSize='24' color="#000" iconColor="red" height="80"/>
```

                  