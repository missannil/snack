// components/h-tabPage/h-tabPage.js
Component({

  properties:{
    GoodsCategoryList:Array,
      _bgColor:String,
      _color:String,
  },
  /**
   * 组件的初始数据
   */
  data: {
    scrollId:0,
    active:0
  },
  /**muted
   * 组件的方法列表
   */
  methods: {
    changeActive(e) {
      let active = e.currentTarget.dataset.active
      let left = 0
      if (active >= 3) { //一行大概显示5个view第三个开始偏移
        left = active - 2
      }
      console.log(active)
      this.setData({
        active,
        left,
      })
    },

  },
  lifetimes: {
    attached() {
       
    }
  }
})
