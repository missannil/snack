
export const debounceHandle = (function debounce() {
  let timer = null;
  return function (callback, time) {
    clearTimeout(timer)
    timer = setTimeout(function () {
      callback()
    }, time)
  }
}())
