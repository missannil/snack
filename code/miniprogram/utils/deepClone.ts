/**
 * 深度克隆函数，返回克隆的对象，不包含原型链属性
 * @param obj 对象或数组
 */
export function deepClone<T extends { [prop: string]: any }>(obj: T) {

    if (obj instanceof Function || obj === null || obj === undefined) {
        throw new Error('参数不能为函数类型、null、undefined')
    }
    let cloneObj: any = obj instanceof Array ? [] : {}
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) { //Object.prototype.hasOwnProperty.call(obj, key) 要自身属性
            if (typeof obj[key] === 'function' || Object.prototype.toString.call(obj[key]).slice(8,-1)=== 'RegExp')  {

                cloneObj[key] = obj[key]

            }
            else if (typeof obj[key] === 'object') {

                cloneObj[key] = deepClone(obj[key])
            } else {
                cloneObj[key] = obj[key]
            }
        }
    }
    return cloneObj
}
