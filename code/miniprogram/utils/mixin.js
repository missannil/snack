function mergeLifetimes(lifeName, option, baseOpt, type) {
    if (type === 'page') {
        const temp = option[lifeName]
        option[lifeName] = function (e) {
            baseOpt[lifeName].call(this, e)
            temp && temp.call(this, e)
        }
    } else {
        option.lifetimes = option.lifetimes || {}
        const temp = option.lifetimes[lifeName]
        option.lifetimes[lifeName] = function (e) {
            baseOpt.lifetimes[lifeName].call(this, e)
            temp && temp.call(this, e)
        }
    }
}

function merge(baseOpt, option, type) {
    if (type === 'page') {
        //合并方法
        for (const prop in baseOpt) {
            //合并函数
            if (baseOpt[prop] instanceof Function) {
                //合并生命周期函数
                const lifetimesArr = ['onLoad', 'onShow', 'onReady']
                const lifeName = lifetimesArr.find(ele => ele === prop)
                if (lifeName) {
                    mergeLifetimes(lifeName, option, baseOpt, type)
                } else if (option[prop]) {
                    //基础配置有这个方法了 报错
                    throw Error(`minxin中有${prop}这个方法了`)
                } else {
                    //合并普通方法
                    option[prop] = baseOpt[prop]
                }
            } else if (baseOpt[prop] instanceof Array) {
                option[prop] = option[prop] || [].concat(baseOpt[prop])
            } else if (baseOpt[prop] instanceof Object) {
                //合并其他对象
                // option[prop] = Object.assign(option[prop] || {}, baseOpt[prop])
                option[prop] = option[prop] || {}
                for (const key in baseOpt[prop]) {
                    if (option[prop][key]) {
                        throw Error(`mixin中的data包含${key}项`);
                    } else {
                        option[prop][key] = baseOpt[prop][key]
                    }
                }
            }
        }
    } else {
        //合并component   
        if (baseOpt.data) {
            option.data = option.data || {}
            for (const prop in baseOpt.data) {
                if (option.data[prop]) {
                    throw Error(`mixin中的data包含${prop}项`);
                } else {
                    option.data[prop] = baseOpt.data[prop]
                }
            }
        }
        if (baseOpt.behaviors) {
            option.behaviors = option.behaviors || []
            option.behaviors = [...option.behaviors, ...baseOpt.behaviors]
        }
        if (baseOpt.methods) {
            option.methods = option.methods || {}
            for (const prop in baseOpt.methods) {
                if (option.methods[prop]) {
                    throw Error(`mixin中的methods包含${prop}方法`);
                } else {
                    option.methods[prop] = baseOpt.methods[prop]
                }
            }
        }
        if (baseOpt.options) {
            option.options = option.options || {}
            for (const prop in baseOpt.options) {
                if (option.options[prop]) {
                    throw Error(`mixin中的options包含${prop}配置`);
                } else {
                    option.options[prop] = baseOpt.options[prop]
                }
            }
        }
        if (baseOpt.lifetimes) {
            for (const name in baseOpt.lifetimes) {
                const lifetimesArr = ['attached', 'detached']
                const lifeName = lifetimesArr.find(ele => ele === name)
                if (lifeName) {
                    mergeLifetimes(lifeName, option, baseOpt, type)
                } else {
                    throw Error(`组件生命周期有${name}么???????`)
                }
            }
        }
    }
    return option
}
export function mixin(baseOpt, type = 'page') {
    if (Object.prototype.toString.call(baseOpt).includes('Object')) {
        if (type === 'page') {
            const originPage = Page
            Page = function (option) {
                originPage(merge(baseOpt, option, type))
            }
        } else if (type === 'component') {
            const originComponent = Component
            Component = function (option) {
                originComponent(merge(baseOpt, option, type))
            }
        }
        else {
            throw `${type} must be 'Page' or 'Component'`
        }
    } else {
        throw TypeError`${baseOpt} must be an object type`
    }
}

