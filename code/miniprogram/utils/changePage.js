export function changePage(arr) {
    const myPage = Page //保留原始Page
    Page = function (options) { //重新定义Page options是传进来的配置对象
        let { onLoad } = options //保存原始options.onLoad
        //把所有arr内的属性加到options.data上面,方法加到options里面
        const once = {} //等待once的函数
        arr.forEach(ele => {
            const item = ele[0] //函数名或者数据
            const value = ele[1]  //函数参数
            if (typeof item === 'function') {
                options[item.name] = item

                if (value) {
                    once[item.name] = value
                }
            } else if (typeof item === 'string') {
                options.data = options.data || {}
                options.data[item] = value
            }
        })
        options.onLoad = function (e) {//重新定义options.onload
            for (const funcName in once) {
                this[funcName](once[funcName])// 运行一次func带参数
            }
            onLoad && onLoad.call(this, e) //再运行原始onload
        }
        return myPage.call(this, options)//最终还是运行原来的Page
    }

}




