// import { deepClone } from "./deepClone";

// 定义一个混合组件函数，传入的参数类型和Component一致
 export let mixinComponent:WechatMiniprogram.Component.Constructor = function (mixinOpt):string {
     //保存原始Component
    let originComponent = Component;
    //重新定义Component
   Component = function (opt) {
       //把建立Component的配置对象和混入的配置对象合并
        let newOpt = mergeOpt( opt,mixinOpt)
        //返回 原始Component 参数为混合完毕的新配置对象
        return originComponent(newOpt)
    }
    //为了满足函数返回类型需要 返回String
    return ''
}
function mergeLifetimes(curOptFunc: any, mixinLifetimesFunc: any,curOpt:any,lifetimesName:any) {
    const temp = curOptFunc
    curOpt[lifetimesName] = function (e: any) {
        mixinLifetimesFunc.call(this,e)
        temp.call(this,e)
    }
}
/**
 * 
 * @param curOpt 要混入的配置对象
 * @param mixinOpt 创立组件时候输入的配置对象
 * @param namespace 为了提示时候的命名空间
 */
function mergeOpt(curOpt: any, mixinOpt: any, namespace = ''){
    //深度克隆mixinOpt是因为 他是个引用值，都在这个对象上面做混入，会冲突的。
    let keys = Object.keys(mixinOpt)
    namespace = namespace || 'mixinObj'
    keys.forEach((propName) => {
        if (!curOpt.hasOwnProperty(propName)) {//如果没有这个属性
            curOpt[propName] = mixinOpt[propName] //直接赋值过去
        }
        else {//有这个属性的情况下
            if (propName === 'behaviors' || propName === 'externalClasses') {//这个属性是behaviors 合并
                curOpt[propName] = [...new Set([...curOpt[propName], ...mixinOpt[propName]])]
            } else if (propName === 'lifetimes') { //这个属性是lifetimes 合并
                var lifenameArr = Object.keys(mixinOpt.lifetimes); //要合并的生命周期函数
                lifenameArr.forEach(ele => {
                    if (!curOpt.lifetimes[ele]) { //如果原来没有,直接赋值过去,
                        curOpt.lifetimes[ele] = mixinOpt.lifetimes[ele];
                    }
                    else { //如果原来有 合并在一起
                        mergeLifetimes(curOpt.lifetimes[ele], mixinOpt.lifetimes[ele],curOpt.lifetimes,ele);
                    }
                })
            }
            else if (typeof mixinOpt[propName] !== 'object'|| propName === 'pureDataPattern') {//如果不是对象
                throw Error(`${namespace}中有${propName}这个属性了`)
            }
            else { //这个属性是对象 递归
                curOpt[propName] = mergeOpt(curOpt[propName], mixinOpt[propName], namespace + '.' + propName)
            }
        }
    })
    return curOpt
}
