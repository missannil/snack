let waitCallbackMap = new Map() //等待回调数据的页面集合
let globalData = null; //App下的globalData,调用内部函数的时候传值,避免app实例未建立时调用出错。
/**
 * 获取全局数据函数
 * @param {Object} _this 要获取数据的页面实例 一般传this
 * @param {Array} dataNameArr 数组每项为一个globalData下的键 多级目录写成'userInfo.userName'
 */
export function getGlobalData(_this, dataNameArr, callbackFunc = null) {
  // 报错处理 
  if (_this.__wxExparserNodeId__ === 'undefined') {
    throw `${_this} must be an instance object `
  }
  if (!Array.isArray(dataNameArr) && dataNameArr.length >= 1) {
    throw `${dataNameArr} is not an array type,the length has to be greater than 1 `
  }
  if (callbackFunc !== null && typeof callbackFunc !== 'function') {
    throw `${callbackFunc} is not an function type`
  }
  if (dataNameArr.length > 1 && typeof callbackFunc === 'function') {
    throw '使用回调函数必须保证获取数据的数组长度为1'
  }
  if (globalData === null) {
    globalData = getApp().globalData //给全局变量赋值 此时赋值确保App页面构建完毕
  }
  //dataNameArr数组的处理 比如dataNameArr = ['cart','userInfo.userName']
  dataNameArr.forEach(item => {
    const itemPathArr = item.split('.') //  ['cart']  ['userInfo','userName']
    const itemName = itemPathArr[itemPathArr.length - 1] // 最后项是itemName
    let itemValue = globalData[itemPathArr[0]] //初始值
    for (let i = 1; i < itemPathArr.length; i++) { //获取itemValue值,itemPathArr长度为1时不会运行
      if (itemValue) {
        itemValue = itemValue[itemPathArr[i]]
      } else {
        itemValue = null
        break;
      }
    }
    if (itemValue) { //有值
      if (typeof callbackFunc === 'function') { //有回调函数
        callbackFunc(itemValue)
      } else {
        _this.setData({
          [itemName]: itemValue
        })
      }
    } else { //没有value值
      if (!waitCallbackMap.has(_this)) { //map里没有这个this
        waitCallbackMap.set(_this, new Map()) //加入一个条目
      }
      waitCallbackMap.get(_this).set(item, callbackFunc) //callbackFunc可能是null或函数
    }
  })
}
/**
 *
 * @param {[]} dataNameArr  异步返回的数据集合 例如 callback(['userInfo','cart'])
 */
export function dataBack(dataNameArr) {
  const dataNameSet = new Set(dataNameArr)
  for (const [instance, map] of waitCallbackMap) {
    const obj = {} //统一 setData会改善性能
    for (const [path, callbackFunc] of map) {
      const itemPathArr = path.split('.') //['userInfo','userName']
      const globalDataName = itemPathArr[0] //globalData属性名'userInfo'
      const itemName = itemPathArr[itemPathArr.length - 1] //'userName'
      if (dataNameSet.has(globalDataName)) {
        let itemValue = globalData[itemPathArr[0]] //初始值
        for (let i = 1; i < itemPathArr.length; i++) { //获取itemValue值,itemPathArr长度为1时不会运行
          if (itemValue) {
            itemValue = itemValue[itemPathArr[i]]
          } else {
            itemValue = null
            break;
          }
        }
        if (callbackFunc) {
          callbackFunc(itemValue)
        } else {
          obj[itemName] = itemValue
          map.delete(path)

          map.size === 0 && waitCallbackMap.delete(instance) //当页面要放回的数据为空时,删除这个页面 避免下次循环性能影响
        }
      }
    }
    if (Object.keys(obj).length) {
      instance.setData(obj) //同一个页面 一起setData数据
    }
  }
}