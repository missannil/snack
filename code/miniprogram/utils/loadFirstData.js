// 数据加载模块 程序启动时，加载必要的项目数据，配合callback回调给页面
// 加载缓存 -- 有 --  加载  --验证缓存timestamp(请求update--有--更新数据和缓存，没有拉倒)
// 没有 -- 云端请求 -- 异步返回 缓存本地
import { getStorageSync, setStorageSync } from './storageSync'
import { dataBack } from './getGlobalData'
let globalData;
export async function loadFirstData(_globalData) {
  globalData = _globalData
  wx.showLoading({ title: '数据加载中' })
  // 读取本地缓存
  const res = getStorageSync('firstData')

  if (res.data) {
    globalData = Object.assign(globalData, res.data)//合并数据到globalData
    wx.hideLoading()
    console.log(`缓存firstData数据加载完毕`)
    // 验证数据
    checkData({
      col: 'projectData',
      _id: 'firstData',
      timestamp: res.data.timestamp
    })//验证缓存是否为最新数据
  } else if (res.data === '') {
    console.log(`本地没有firstData缓存`)
    getCloudData()
  } else {
    console.log('读取本地缓存出错', data.error)
  }
}
async function getCloudData() {
  const res = await wx.cloud.callFunction({
    name: `get_firstData`
  })
  if (res.result.data) {
    updateData(res.result.data)
    console.log(`获取云端firstData数据完毕`)
    wx.hideLoading()
  } else {
    wx.hideLoading()
    wx.showToast({
      title: '无法获取远程服务器数据,请联系客服人员',
      icon: 'none',
      mask: true,
      duration: 100000
    })
  }
}
async function checkData(config) {
  // 验证数据
  console.log(`验证缓存firstData数据是否最新`)
  const res = await wx.cloud.callFunction({
    name: 'checkData',
    data: config
  })
  console.log(`返回缓存firstData验证数据`)
  if (res.result) {
    console.log('发现更新', res)
    wx.showLoading({
      title: '数据更新中'
    })
    updateData(res.result)
    wx.hideLoading()
    console.log(`数据firstData更新完毕`)

  } else {
    console.log(`无firstData数据更新`)
  }
}
function updateData(data) {
  delete data._id //无用的参数，可以在云函数聚合 aggregate删除的
  globalData = Object.assign(globalData, data)//替换原来的数据
  setStorageSync('firstData', data) //更新缓存数据
  delete data.timestamp //对于下面是无用的
  dataBack(Object.keys(data))
}