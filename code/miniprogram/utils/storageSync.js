
export function getStorageSync(key) {
    try {
        const data = wx.getStorageSync(key)
        return {
            data,
            error: ''
        }
    } catch (error) {
        return {
            data: false,
            error
        }
    }
}
export function setStorageSync(key, value) {
    try {
        wx.setStorageSync(key, value)
        return true
    } catch (error) {
        console.log(`set缓存${key}的值出错:`, error)
        return false
    }
}