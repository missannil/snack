import { themeList } from '../config/themeList'
export function setTheme  (theme_id = 'default') {
    if (typeof theme_id !== 'string') {
        throw `${theme_id} must be an string type`
    }
    this.setData({
        themeColor: themeList.find(ele => ele.id === theme_id).color 
    })
}

