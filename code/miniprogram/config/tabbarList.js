export const tabbarList = [{
        "title": "熙熙食坊",
        "name": "首页",
        "icon": {
            "default": "i-home",
            "selected": "i-home-selected"
        }
    },
    {
        "title": "分类",
        "name": "分类",
        "icon": {
            "default": "i-category",
            "selected": "i-category-selected"
        }
    },
    {
        "title": "会员",
        "name": "会员",
        "icon": {
            "default": "i-huiyuan",
            "selected": "i-huiyuan-selected"
        }
    },
    {
        "title": "我的",
        "name": "我的",
        "icon": {
            "default": "i-mine",
            "selected": "i-mine-selected"
        }
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiYmFyTGlzdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRhYmJhckxpc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLE1BQU0sVUFBVSxHQUFHLENBQUM7UUFDekIsT0FBTyxFQUFFLE1BQU07UUFDZixNQUFNLEVBQUUsSUFBSTtRQUNaLE1BQU0sRUFBRTtZQUNKLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFVBQVUsRUFBRSxpQkFBaUI7U0FDaEM7S0FDRjtJQUNEO1FBQ0UsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsSUFBSTtRQUNaLE1BQU0sRUFBRTtZQUNKLFNBQVMsRUFBRSxZQUFZO1lBQ3ZCLFVBQVUsRUFBRSxxQkFBcUI7U0FDcEM7S0FDRjtJQUNEO1FBQ0UsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsSUFBSTtRQUNaLE1BQU0sRUFBRTtZQUNKLFNBQVMsRUFBRSxXQUFXO1lBQ3RCLFVBQVUsRUFBRSxvQkFBb0I7U0FDbkM7S0FDRjtJQUNEO1FBQ0UsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsSUFBSTtRQUNaLE1BQU0sRUFBRTtZQUNKLFNBQVMsRUFBRSxRQUFRO1lBQ25CLFVBQVUsRUFBRSxpQkFBaUI7U0FDaEM7S0FDRjtDQUNBLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgdGFiYmFyTGlzdCA9IFt7XHJcbiAgXCJ0aXRsZVwiOiBcIueGmeeGmemjn+WdilwiLFxyXG4gIFwibmFtZVwiOiBcIummlumhtVwiLFxyXG4gIFwiaWNvblwiOiB7XHJcbiAgICAgIFwiZGVmYXVsdFwiOiBcImktaG9tZVwiLFxyXG4gICAgICBcInNlbGVjdGVkXCI6IFwiaS1ob21lLXNlbGVjdGVkXCJcclxuICB9XHJcbn0sXHJcbntcclxuICBcInRpdGxlXCI6IFwi5YiG57G7XCIsXHJcbiAgXCJuYW1lXCI6IFwi5YiG57G7XCIsXHJcbiAgXCJpY29uXCI6IHtcclxuICAgICAgXCJkZWZhdWx0XCI6IFwiaS1jYXRlZ29yeVwiLFxyXG4gICAgICBcInNlbGVjdGVkXCI6IFwiaS1jYXRlZ29yeS1zZWxlY3RlZFwiXHJcbiAgfVxyXG59LFxyXG57XHJcbiAgXCJ0aXRsZVwiOiBcIuS8muWRmFwiLFxyXG4gIFwibmFtZVwiOiBcIuS8muWRmFwiLFxyXG4gIFwiaWNvblwiOiB7XHJcbiAgICAgIFwiZGVmYXVsdFwiOiBcImktaHVpeXVhblwiLFxyXG4gICAgICBcInNlbGVjdGVkXCI6IFwiaS1odWl5dWFuLXNlbGVjdGVkXCJcclxuICB9XHJcbn0sXHJcbntcclxuICBcInRpdGxlXCI6IFwi5oiR55qEXCIsXHJcbiAgXCJuYW1lXCI6IFwi5oiR55qEXCIsXHJcbiAgXCJpY29uXCI6IHtcclxuICAgICAgXCJkZWZhdWx0XCI6IFwiaS1taW5lXCIsXHJcbiAgICAgIFwic2VsZWN0ZWRcIjogXCJpLW1pbmUtc2VsZWN0ZWRcIlxyXG4gIH1cclxufVxyXG5dIl19