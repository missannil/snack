

export const shopInfo = {
    "community":'生活小区',
    "address": "盘锦市兴隆台区开发区生活小区",
    "latitude": 41.14532253689236,
    "longitude": 122.05853488498263,
    "markers": [{
        "id": 100,
        "latitude": 41.14532253689236,
        "longitude": 122.05853488498263,
        "width": 40,
        "height": 40,
        "iconPath": "./shop.png",
        "scoped": 500,
        "count": 5,
        "callout": {
            "content": "熙熙食坊",
            "color": "#f40",
            "fontSize": 20,
            "display": "ALWAYS"
        }
    }]
  }