export const tabbarList = [{
  "title": "熙熙食坊",
  "name": "首页",
  "icon": {
      "default": "i-home",
      "selected": "i-home-selected"
  }
},
{
  "title": "分类",
  "name": "分类",
  "icon": {
      "default": "i-category",
      "selected": "i-category-selected"
  }
},
{
  "title": "会员",
  "name": "会员",
  "icon": {
      "default": "i-huiyuan",
      "selected": "i-huiyuan-selected"
  }
},
{
  "title": "我的",
  "name": "我的",
  "icon": {
      "default": "i-mine",
      "selected": "i-mine-selected"
  }
}
]