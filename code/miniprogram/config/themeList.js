export const themeList = [
    {
        id: 'default',
        name: '默认',
        color: `
    --darktheme:#117c3c;
    --theme: #18b357;
    --white: #f5f5f5;
    --red: #f40;
    --lightgray: #eee;
    --ddd:#ddd;
    --gray: #aaa;
    --lightblack: #666;
    --black: #333;
    `
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWVMaXN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidGhlbWVMaXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE1BQU0sQ0FBQyxNQUFNLFNBQVMsR0FBRztJQUN2QjtRQUNFLEVBQUUsRUFBRSxTQUFTO1FBQ2IsSUFBSSxFQUFFLElBQUk7UUFDVixLQUFLLEVBQUU7Ozs7Ozs7Ozs7S0FVTjtLQUNGO0NBQ0YsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8vIOaVsOe7hDDpobnlgZrkuLrpu5jorqTpobnnm64gYWN0aXZlOnRydWUgXHJcbmV4cG9ydCBjb25zdCB0aGVtZUxpc3QgPSBbXHJcbiAge1xyXG4gICAgaWQ6ICdkZWZhdWx0JyxcclxuICAgIG5hbWU6ICfpu5jorqQnLFxyXG4gICAgY29sb3I6IGBcclxuICAgIC0tZGFya3RoZW1lOiMxMTdjM2M7XHJcbiAgICAtLXRoZW1lOiAjMThiMzU3O1xyXG4gICAgLS13aGl0ZTogI2Y1ZjVmNTtcclxuICAgIC0tcmVkOiAjZjQwO1xyXG4gICAgLS1saWdodGdyYXk6ICNlZWU7XHJcbiAgICAtLWRkZDojZGRkO1xyXG4gICAgLS1ncmF5OiAjYWFhO1xyXG4gICAgLS1saWdodGJsYWNrOiAjNjY2O1xyXG4gICAgLS1ibGFjazogIzMzMztcclxuICAgIGBcclxuICB9XHJcbl1cclxuIl19