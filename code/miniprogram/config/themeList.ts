// 数组0项做为默认项目 active:true 
export const themeList = [
  {
    id: 'default',
    name: '默认',
    color: `
    --darktheme:#117c3c;
    --theme: #18b357;
    --white: #f5f5f5;
    --red: #f40;
    --lightgray: #eee;
    --ddd:#ddd;
    --gray: #aaa;
    --lightblack: #666;
    --black: #333;
    `
  }
]
