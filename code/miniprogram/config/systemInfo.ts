const systemInfo = wx.getSystemInfoSync();
const capsuleInfo = wx.getMenuButtonBoundingClientRect();
const statusBarHeight = systemInfo.statusBarHeight;
const customNavHeight = capsuleInfo.top + capsuleInfo.bottom - systemInfo.statusBarHeight;
const capsuleRight = systemInfo.screenWidth - capsuleInfo.right 
const capsulePadding= capsuleInfo.top - statusBarHeight
const capsuleHeight = capsuleInfo.height;
const capsuleTop = capsuleInfo.top
export {statusBarHeight, customNavHeight ,capsulePadding,capsuleRight,capsuleHeight,capsuleTop,systemInfo};
